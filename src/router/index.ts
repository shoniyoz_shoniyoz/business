import { createRouter, createWebHistory } from "vue-router";

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: "/login",
      name: "login",
      component: () => import("../pages/auth/Login.vue"),
      meta: {
        layout: "default",
      },
    },
    {
      path: "/",
      name: "home",
      component: () => import("../pages/home/Home.vue"),
      meta: {
        layout: "admin",
      },
    },

    {
      path: "/news",
      name: "news",
      component: () => import("../pages/news/News.vue"),
      meta: {
        layout: "admin",
      },
    },
  ],
});

export default router;

router.beforeEach((to, from, next) => {
  const hasToken = localStorage.getItem("access-token");

  if (to.name === "login" && hasToken) {
    next({ name: "home" });
  } else if (to.name !== "login" && !hasToken) {
    next({ name: "login" });
  } else {
    next();
  }
});
