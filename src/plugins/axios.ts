import axios, { Axios } from "axios";
const $axios = axios.create({
  baseURL: "https://biznesportali.uz/gateway/",
  // headers: {},
});

export default $axios;

$axios.interceptors.request.use((config) => {
  if (localStorage.getItem("access-token")) {
    config.headers["Authorization"] =
      "Bearer " + localStorage.getItem("access-token");
  }
  return config;
});
