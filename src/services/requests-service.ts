import BaseApiService from "./base-service";

class RequsetApiService extends BaseApiService {
  async getAll(url: string, params?: any) {
    let response = await this.$axios.get(url, params);
    return response;
  }

  async post<T>(url: string, data: T) {
    let response = await this.$axios.post(url, data);
    return response;
  }
}

export default RequsetApiService;
const requsetApiService = new RequsetApiService();
export { RequsetApiService, requsetApiService };
