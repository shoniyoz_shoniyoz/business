import BaseApiService from "./base-service";

class FileApiService extends BaseApiService {
  async post(url: string, data: any) {
    let response = await this.$axios.post(url, data);
    return response;
  }

  async remove(url: string, name: string) {
    let response = await this.$axios.delete(url, {
      data: {
        filePath: name,
      },
    });
    return response;
  }
}

export default FileApiService;

const fileApiService = new FileApiService();
export { FileApiService, fileApiService };
