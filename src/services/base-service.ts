import $axios from "../plugins/axios";

class BaseApiService {
  $axios = $axios;
  baseUrl?: any = null;
  constructor(baseUrl?: string | undefined) {
    this.baseUrl = baseUrl;
  }
}

export default BaseApiService;
const baseApiService = new BaseApiService("/");
export { baseApiService };
